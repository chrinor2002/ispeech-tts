# ispeech-tts
A demo of using ffmpeg to trim audio within a docker container. This was an experiment to see if there was a way to use a existing web service to grab audio, trim it, then output the data all within a docker container.

NOTE: this is a proof of concept, it is not intended to be used to circumvent the copyrights or functionality of ispeech. If you like their service, use their SDK. 

## Usage

```
docker build -t chrinor2002/ispeech-tts .
docker run --rm chrinor2002/ispeech-tts "text to be translated to audio" > output.mp3
```

## Example of playing file:

```
# standard OSX TTS
say "hello world"

# using chrinor2002/ispeech-tts
docker run --rm chrinor2002/ispeech-tts "hello world" > helloworld.mp3
aplay helloworld.mp3
```

