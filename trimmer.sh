#!/bin/sh

TEXT="$1"
PWD=/working

# grab the results if there was no text
curl -s -o $PWD/powered.mp3 -G 'http://www.ispeech.org/p/generic/getaudio?voice=ukenglishfemale&speed=0&action=convert' --data-urlencode "text=" --compressed

# grab the text
curl -s -o $PWD/text.mp3 -G 'http://www.ispeech.org/p/generic/getaudio?voice=ukenglishfemale&speed=0&action=convert' --data-urlencode "text=$TEXT" --compressed

# compute times
TIME=$(ffprobe -loglevel quiet -of compact=p=0:nk=1 -show_entries format=duration $PWD/text.mp3)
TRIM=$(ffprobe -loglevel quiet -of compact=p=0:nk=1 -show_entries format=duration $PWD/powered.mp3)
TRIMMED=$(echo "$TIME - $TRIM" | bc -l)

# trim text
ffmpeg -loglevel quiet -i $PWD/text.mp3 -t $TRIMMED -acodec copy $PWD/tmp.mp3

# output
cat $PWD/tmp.mp3

# cleanup
rm $PWD/powered.mp3 $PWD/text.mp3 $PWD/tmp.mp3
