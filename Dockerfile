FROM jrottenberg/ffmpeg
RUN apt-get -y update
RUN apt-get -y install bc curl
RUN mkdir /working
COPY trimmer.sh /trimmer.sh
ENTRYPOINT ["/bin/bash", "/trimmer.sh"]
